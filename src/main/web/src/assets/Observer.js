const listeners = {};

const normalizeKey = function(key) {
    return key == null ? '' : (''+key).trim();
}

export default {

    listen(key, listener) {

        if ( typeof listener != 'undefined' && typeof listener != 'function' ) {
            throw new Error('listener must be of type exception');
        }

        key = normalizeKey(key);

        if ( typeof listeners[key] == 'undefined' ) {
            listeners[key] = [];
        }

        listeners[key].push(listener);
    },

    inform(key, ...values) {

        key = normalizeKey(key);

        console.log('inform', key, values);

        if ( typeof listeners[key] == 'undefined' ) {
            console.log('no listeners for key='+ key);
            return;
        }

        listeners[key].forEach( listener=>{
            listener(...values);
        } );
    }
};