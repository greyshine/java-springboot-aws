import {createApp} from 'vue'
import App from '@/App.vue'
import router from '@/router'
import axios from 'axios'
import observer from '@/assets/Observer.js'

import '@/assets/styles.css'

axios.defaults.baseURL = window.location.protocol + "//" + window.location.hostname + ":8080";
console.log('axios.defaults.baseURL',axios.defaults.baseURL);
axios.defaults.headers.common['token'] = localStorage.getItem('token');
console.log('current token', localStorage.getItem('token'));

const app = createApp(App)
    .use(router)
    .mount('#app');

app.$root.isLoggedIn = function() {
    let token = localStorage.getItem('token');
    return token != null;
};

observer.listen('login', token=>{
    axios.defaults.headers.common['token'] = token;
    localStorage.setItem('token', token);
} );

observer.listen('logout', ()=>{
    axios.defaults.headers.common['token'] = null;
    localStorage.removeItem('token');
} );



console.log('isLoggedIn', app.$root.isLoggedIn(), localStorage.getItem('token'));

