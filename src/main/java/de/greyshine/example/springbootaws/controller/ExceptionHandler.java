package de.greyshine.example.springbootaws.controller;

import de.greyshine.example.springbootaws.services.IllegalPasswordException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
@Slf4j
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(IllegalPasswordException.class)
    public void handleIllegalPasswordException(IllegalPasswordException exception, HttpServletResponse response) throws IOException {
        log.info("{}", exception.getMessage());
        response.sendError(HttpStatus.UNAUTHORIZED.value(), exception.getMessage());
    }
}
