package de.greyshine.example.springbootaws.controller;

import de.greyshine.example.springbootaws.annotations.User;
import de.greyshine.example.springbootaws.services.DataService;
import de.greyshine.example.springbootaws.services.UserService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
@Slf4j
public class Controller {

    public static final String HEADER_TOKEN = "TOKEN";

    private final UserService userService;
    private final DataService dataService;

    Controller(final UserService userService, final DataService dataService) {
        this.userService = userService;
        this.dataService = dataService;
    }

    @GetMapping(value="/list", produces = APPLICATION_JSON_VALUE)
    public List<String> entries() {
        return dataService.getEntries();
    }

    @PostMapping(value="/entry")
    public void newEntry(@User String user, @RequestBody EntryRequest entryRequest) {

        var entry = LocalDateTime.now().format( DateTimeFormatter.ISO_DATE_TIME )
                +", "+ user +": "+ entryRequest.getEntry();

        dataService.addEntry( entry );
    }

    @PostMapping(value = "/login", produces = TEXT_PLAIN_VALUE)
    public String login(@RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        return userService.doLogin( loginRequest.getUser(), loginRequest.getPassword() );
    }

    /**
     * Axios on Javascript site does not allow 'logout' as url part!
     *
     */
    @PostMapping(value = "/log-out")
    public void logout(@RequestBody LogoutRequest logoutRequest, HttpServletRequest request) {
        log.info("LOGOUT {}, {}", request.getRemoteAddr(), logoutRequest);
        userService.logoutByToken( request.getHeader( HEADER_TOKEN ) );
    }


    @GetMapping(value = "loggedIn/{token}")
    public boolean isLoggedIn(@PathParam(value="token") String token, HttpServletRequest request) {
        return userService.getUserByToken(token) != null;
    }

    @Data
    public static class LoginRequest {
        private String user;
        private String password;
    }

    @Data
    public static class LogoutRequest {
        private String token;
    }

    @Data
    public static class EntryRequest {
        private String entry;
    }
}
