package de.greyshine.example.springbootaws.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class UserService {

    // key=token; value=login
    private final Map<String, String> logins = new ConcurrentHashMap<>();

    /**
     * @param user user to be logged in
     * @param password of user to be logged in
     * @return the token if the user is logged in otherwise null
     */
    public String doLogin(String user, String password) {

        Assert.isTrue(user != null && !user.isBlank(), "Parameter user must not be blank");
        Assert.isTrue(user.equals(user.trim()), "Parameter user must not start/end with blanks");

        if (!"password".equalsIgnoreCase(password)) {
            throw new IllegalPasswordException(user);
        }

        final var token = UUID.randomUUID().toString();

        // TODO actually check if user exists

        logins.put(token, user);

        return token;
    }

    public void logoutByToken(String token) {
        this.logins.remove(token);
    }

    public String getUserByToken(String token) {
        return logins.get(token);
    }
}
