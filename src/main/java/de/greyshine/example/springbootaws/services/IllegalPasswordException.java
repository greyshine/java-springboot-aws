package de.greyshine.example.springbootaws.services;

public class IllegalPasswordException extends RuntimeException {

    public IllegalPasswordException(String user) {
        super("Bad password for user '"+ user +"'");
    }
}
