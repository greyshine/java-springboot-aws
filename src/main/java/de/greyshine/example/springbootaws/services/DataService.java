package de.greyshine.example.springbootaws.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataService {

    final List<String> entries = new ArrayList<>();

    public List<String> getEntries() {
        return entries;
    }

    public void addEntry(String entry) {

        if ( entry == null || entry.trim().isEmpty() ) {
            return;
        }

        entries.add( entry.trim() );
    }
}
